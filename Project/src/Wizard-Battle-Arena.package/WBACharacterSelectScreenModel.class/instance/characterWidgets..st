screen building
characterWidgets: cood
	"Loads all 13 available character icons."
	^ (1 to: 13) collect: [ :i |
			| sprites |
			sprites := WBACharacterDescriptor new
				fileName: 'perso' , i printString;
				position: cood;
				loadShortSpritesWith: screen surface .
			self newAnimatedWidget: sprites ]