rendering
startAnimation
	"Starts animation. Characters on this select Model appear to be moving."
	self every10GameTickDo: [
		characterWidget widgets do: [ :w |
			w widgets do: [ :toggleSp |
				toggleSp toggle ] ] ] 