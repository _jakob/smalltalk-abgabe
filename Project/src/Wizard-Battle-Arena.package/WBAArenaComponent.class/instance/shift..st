accessing
shift: anObject
	"distance to shift the sprite from cell top left corner while rendering"
	shift := anObject.
	negatedShift := shift negated.