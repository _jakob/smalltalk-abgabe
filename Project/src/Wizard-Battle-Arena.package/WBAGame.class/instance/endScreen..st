end
endScreen: winner
	"Loads the endscreen model and passes it the player that won the game."
	WBAEndScreenModel new
		screen: screen;
		startingGame: self;
		winner: winner;
		display