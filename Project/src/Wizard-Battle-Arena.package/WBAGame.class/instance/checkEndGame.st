end
checkEndGame
	"Check whether or not there are enough active players or if the remaining living player won."
	| alivePlayers |
	alivePlayers := players reject: [ :each | each character isDead ].
	alivePlayers size = 1 ifTrue: [ self endScreen: alivePlayers first ]