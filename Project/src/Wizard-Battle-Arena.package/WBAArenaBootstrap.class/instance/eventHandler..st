bootstrap
eventHandler: screen
	"Set a new eventHandler. Must be of type WBAArenaEventHandler. This handles the key inputs."
	^ WBAArenaEventHandler new
		players: startGame players;
		schedulers: { startGame scheduler . screen scheduler };
		yourself