cell bootstrap
bootstrapGroundOnlyCell: cell
	"cell must be of type WBACell. Initializes a cell that is empty."
	cell element: WBAEmpty empty.
	cell ground: grounds atRandom
	