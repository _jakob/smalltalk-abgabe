cell bootstrap
bootstrapCharacterCell: cell playerIndex: index
	"Bootstraps the cell where an character will be standing at the beginning of the game."
	| character |
	character := characters at: index.
	cell ground: grounds first.
	cell element: character.
	(startGame players at: index) character: character.
	character cell: cell.
	character player: (startGame players at: index).
	character spell: (spells at: index).
	character game: startGame