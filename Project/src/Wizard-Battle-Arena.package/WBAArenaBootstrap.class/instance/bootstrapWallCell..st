cell bootstrap
bootstrapWallCell: cell
	"Cell must be of type WBACell. Initializes a cell with a wall."
	cell element: walls atRandom.
	cell ground: grounds first