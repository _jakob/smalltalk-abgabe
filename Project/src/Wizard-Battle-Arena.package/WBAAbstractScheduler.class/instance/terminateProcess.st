terminate
terminateProcess
	"Terminate the current process."
	[ process terminate ] on: Error do: [ ].