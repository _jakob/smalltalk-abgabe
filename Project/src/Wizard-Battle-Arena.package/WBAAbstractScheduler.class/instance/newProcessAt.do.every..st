initialization
newProcessAt: priority do: aBlock every: duration
	"Starts a new process which runs the given block every duration.
	Priority must be of type integer, > 0., resembles the priority of the process.
	aBlock must be anything that is executable.
	duration must be a time period, eg. 20 miliseconds. 
	"
	^ [ 
		[ | time delay |
		time := DateAndTime now.
		aBlock value.
		delay := duration - (DateAndTime now - time).
		delay negative 
			ifFalse: [ delay wait ]
			ifTrue: [ "slowing process" ]
		 ] repeat ] forkAt: priority