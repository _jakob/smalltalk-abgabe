animation
bury
	"Call this method when the character died, removes all elements of this character, such as scheduler, cell."
	cell element: WBAEmpty empty.
	scheduler stopGameTickActionFor: self.
	cell := nil.
	game checkEndGame