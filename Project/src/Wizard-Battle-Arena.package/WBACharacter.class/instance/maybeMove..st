animation
maybeMove: index
	"Calculates whether or not the player can move to the given direction. If so, it calls the move function of itself."
	isMoving ifTrue: [ "already moves" ^ self ].
	direction := index. 
	(cell neighbours at: index) element allowMovement ifFalse: [ ^ currentFoot := 0 ].
	self move: index