animation
switchCell: shiftIncr
	"Switch the cell where this character is. Updates all variables, like the cell and the shift for rendering."
	| newCell |
	newCell := cell neighbours at: direction.
	newCell element allowMovement ifTrue: [ 
		cell element: WBAEmpty empty.
		cell := newCell.
		cell element: self.
		cell spells 
			detect: [ :sp | sp character ~= self ] 
			ifFound: [ :elem | elem nextAnimation ].
		self shift: self initialShift - (self numberOfAnimationFrame / 2 * shiftIncr negated).
		^ shiftIncr ].
	"newCell is full, move back"
	^ shiftIncr negated