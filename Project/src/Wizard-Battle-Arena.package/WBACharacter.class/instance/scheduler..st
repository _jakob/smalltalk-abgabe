accessing
scheduler: anObject
	"Must be of (sub-)type WBAAbstractScheduler. Used to add new animations to the scheduler."
	scheduler := anObject