rendering
renderOn: canvas
	"This is called every game tick."
	canvas pathTransform translateBy: shift.
	canvas setPaint: self spriteToRender.
	canvas draw.
	canvas pathTransform translateBy: negatedShift. 