animation
move: index
	"Move the character on the screen. Index resemples the direction. Adds a new MoveAnimation to the scheduler."
	isMoving := true.
	currentFoot := 1.
	scheduler 
		newGameTickAction: (WBACharacterMoveAnimation new
			character: self;
			direction: index;
			yourself)
		for: self 