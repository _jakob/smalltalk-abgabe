rendering
spriteToRender
	"Indicates what sprites should be rendered, based on where the character looks."
	^ sprites at: direction * 3 - 1 + currentFoot 
	