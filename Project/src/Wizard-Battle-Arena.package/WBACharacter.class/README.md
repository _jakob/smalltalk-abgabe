Class representing a given character in the game. 
- Cell must be of type WBACell and resembles the current cell the player is on.
- currentFood must be either 0 or 1. Indicates the character movement on the screen.