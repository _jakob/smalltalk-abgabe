instance creation
backgroundAt: i
	"returns a object of type WBABackgroundDescriptor. I must be an integer between 0 and 5 and resembles a given tileset."
	^ self new
		fileName: 'terrain';
		position: (#(0 48 96 144 192 240) at: i) @ 0 ;
		yourself