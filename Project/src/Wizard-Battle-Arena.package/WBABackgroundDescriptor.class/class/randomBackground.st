instance creation
randomBackground
	"Creates an object of type WBABackgroundDescriptor with a random tileset."
	^ self new
		fileName: 'terrain';
		position: #(0 48 96 144 192 240) atRandom @ 0 ;
		yourself