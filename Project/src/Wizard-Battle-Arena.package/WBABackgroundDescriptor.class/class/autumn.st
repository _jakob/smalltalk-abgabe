instance creation
"Defines the file name of the background tiles in the resources folder. Creates an object of type WBABackgroundDescriptor."
autumn
	^ self new
		fileName: 'terrain';
		position: 48 @ 0 ;
		yourself