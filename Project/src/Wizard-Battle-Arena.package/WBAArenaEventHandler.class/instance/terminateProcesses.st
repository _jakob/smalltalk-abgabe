events
terminateProcesses
	"Kill all proceses when the game ends."
	schedulers do: [ :sc | sc terminateProcess ].