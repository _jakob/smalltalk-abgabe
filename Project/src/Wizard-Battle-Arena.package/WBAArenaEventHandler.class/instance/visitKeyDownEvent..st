events
visitKeyDownEvent: anEvent
	"Catches the keyDownEvent from the OS and passes it through to all the players."
	players do: [ :each |
		each inArenaKeyDown: anEvent scanCode ]