events
visitKeyUpEvent: anEvent
	"Catches the keyUpEvent from the OS and passes it through to all the players."
	players do: [ :each |
		each inArenaKeyUp: anEvent scanCode ]