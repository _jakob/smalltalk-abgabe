constants
minTimeBetweenRendering
	"ms of time period that is between to rendering actions."
	^ (1000 / self maxFrameRate) milliSeconds 