initialization
initialize
	"Initializes the object. gameTickActions is a dictionary pointing from a player to a schedule pipeline."
	super initialize.

	gameTickActions := Dictionary new.
	process := self 
		newProcessAt: 35 
		do: [ gameTickActions keysAndValuesDo: [ :element :animation |
			animation animate ] ]
		every: self animationTick.