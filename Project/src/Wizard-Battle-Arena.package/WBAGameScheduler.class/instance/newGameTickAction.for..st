game tick
newGameTickAction: aBlock for: element
	"Adds a new block for an element that is executed on the next tick."
	gameTickActions at: element put: aBlock