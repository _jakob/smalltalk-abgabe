game tick
stopGameTickActionFor: element 
	"Removes an element from a schedule pipeline for a given element."
	gameTickActions removeKey: element ifAbsent: nil