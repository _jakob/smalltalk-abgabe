rendering
renderWidgetsOn: canvas
	"This is called every animation tick. Load the current status on the screen."
	titleWidget renderOn: canvas translatedBy: 30@30.
	canvas pathTransform translateBy: 220@30.
	playersWidget do: [ :widget |
		widget renderOn: canvas translatedBy: 0@100. ].
	arrowWidget renderOn: canvas translatedBy: -75 @ ((-1 + currentChoice) * 100 + 10)