screen building
newNumPlayersWidget: num
	"This resembles a row widget, num must be an integer > 0, which indicates the value of this row."
	^ self newRowWidget: { (self textWidgetAt: num) . (self textWidgetAt: #player) } space: 40