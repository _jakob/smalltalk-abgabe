actions
endScreenComputation
	| keyCodes |
	"Assigns an instance of WBAPlyer to each available player. Sets the default keyboardCodes for each player."
	keyCodes := WBAPlayer defaultKeyboardCodes.
	startingGame players: ((1 to: currentChoice + 1) collect: [ :i | WBAPlayer new
			keyboardCodes: (keyCodes at: i);
			yourself ])