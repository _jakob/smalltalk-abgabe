initialization
loadWidgets
	"Loads the title, player and arrowWidged."
	titleWidget := self newSingleSpriteWidget: (self textSpriteAt: #title).
	playersWidget := (1 to: 1) collect: [ :i | self newNumPlayersWidget: i ].
	arrowWidget := self newLargeArrowSelectWidget: 300.