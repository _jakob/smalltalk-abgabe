accessing
movementKeyDown: anObject
	"Direction of the current key the player is holding down to handle movement across multiple tiles"
	movementKeyDown := anObject