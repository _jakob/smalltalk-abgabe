animation
startAnimation
	"Starts animation of character movement on the selection menu."
	self every10GameTickDo: [ 
		characterWidget toggle.
		spellWidget toggle ] 