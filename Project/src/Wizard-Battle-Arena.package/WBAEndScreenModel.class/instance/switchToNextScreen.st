actions
switchToNextScreen
	"Starts either the new game or proceeds to the arena select model."
	currentChoice = 0 
		ifTrue: [ self nextScreen: WBAArenaSelectScreenModel ]
		ifFalse: [ 
			startingGame scheduler terminateProcess.
			WBANumberPlayerSelectScreenModel new
				screen: screen;
				display ]