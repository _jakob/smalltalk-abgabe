This is a dialog that is shown when a game has ended. 
It allows the selection of a new game, and if selected starts it.

It also shows who won the last game.