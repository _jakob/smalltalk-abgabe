rendering
renderOn: canvas
	"Called on every animation tick. Paints the wall on the canvas."
	canvas pathTransform translateBy: shift.
	canvas setPaint: sprite.
	canvas draw.
	canvas pathTransform translateBy: negatedShift.