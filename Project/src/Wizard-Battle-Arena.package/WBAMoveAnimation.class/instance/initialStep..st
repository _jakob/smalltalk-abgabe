initialization
initialStep: startStep
	"Indicates the initial step, the foot that sticks in front."
	super initialStep: startStep.
	self movingElement shiftBy: (startStep * shiftIncr)