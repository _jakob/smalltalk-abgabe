shiftIncr
shiftIncrForDirection: index
	"Index resembles the direction. See below for the mappings."
	1 (down) -> 0@ -2.
	2 (left) -> 3@0.
	3 (right) -> -3 @0.
	4 (up) -> 0@2 "
	
	^ index \\ 3 - 1 @ (index // 2 - 1) * self cellSize / self numberOfAnimationFrame