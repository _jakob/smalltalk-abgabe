accessing
movingElement
	"Must be of type WBACharacter. This is the character that is moving."
	self subclassResponsibility 