animating
gameTick
	"This message should be sent, when a game tick has passed."
	self movingElement shiftBy: shiftIncr.
	self isMidAnimation ifTrue: [ self midAnimation ].
	self isEndAnimation ifTrue: [ self movingElement finishMoveAnimation ] 