animating
gameTick
	"Called every tick. If the animation has endet, it sends the bury message to the character."
	(self isAnimationSplit: 8) ifTrue: [ character dieRotation ].
	self isEndAnimation ifTrue: [ character bury ] 