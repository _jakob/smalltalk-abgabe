rendering
renderBackgroundOn: canvas
	"Sets the background of the whole canvas to grey."
	canvas setPaint: Color gray.
	canvas drawShape: (0@0 fastCorner: screen window extent).