accessing
loadAllText
	"Loads all texts from the folder.  Each text is accessable with its key, which can be found in availableText class method."
	| dict |
	dict := Dictionary new.
	self class availableText do: [ :key |
		fileName := key asString.
		dict at: key put: self loadImageFile ].
	^ dict