bootstrap
dispatchIndex: index on: bootstraper with: cell
	"index must be an integer > 0. Cell must be of Type WBACell. Bootstraps a given cell based on an given index. 0 is groundCell, 1 is wallcell, > 1 is player cell."
	index = 0 ifTrue: [ ^ bootstraper bootstrapGroundOnlyCell: cell ].
	index = 1 ifTrue: [ ^ bootstraper bootstrapWallCell: cell ].
	^ bootstraper bootstrapCharacterCell: cell playerIndex: index - 1 