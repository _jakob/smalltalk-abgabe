accessing
expectedNumberOfPlayers
	"Retunrns the number of players on the initialzed map. See class comment for details."
	| max |
	max := 0.
	cellDescriptors do: [ :column |
		column do: [ :descr | 
			max := max max: descr ] ].
	^ max - 1