constants
isMidAnimation
	"Indicates that the halftime of the animation has been reached."
	^ step = (self numberOfAnimationFrame // 2)