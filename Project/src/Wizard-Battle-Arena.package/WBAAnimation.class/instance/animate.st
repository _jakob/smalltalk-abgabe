animating
animate
	"This is called every gameTick. Increments the step counter, which is a counter of every gameTick since the beginning."
	step := step + 1.
	self gameTick