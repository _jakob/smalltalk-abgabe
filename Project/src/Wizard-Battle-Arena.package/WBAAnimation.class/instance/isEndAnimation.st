constants
isEndAnimation
	"Indicates that this particular animation is over."
	^ step = self numberOfAnimationFrame