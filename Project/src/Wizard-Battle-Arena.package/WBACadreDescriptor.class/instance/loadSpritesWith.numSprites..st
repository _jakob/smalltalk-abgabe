as yet unclassified
loadSpritesWith: surface numSprites: num
	"Returns an array of size the given number num (integer between 1 and 13.). Each position represents a different character sprite."
	| sprites |
	sprites := Array new: num.
	1 to: num do: [ :i |
		fileName := 'cadre' , i printString.
		sprites at: i put: self loadImageFile ].
	^ sprites