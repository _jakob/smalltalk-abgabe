Class representing a concrete cell. A cell know its element (must be of type WBAElement), the ground (of Type WBAGround) and the neighbours (type WBACells).
