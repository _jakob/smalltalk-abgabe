actions
switchToNextScreen
	"This defines the next action. Since this is the last selection, the game will be bootstrapped in the next step."
	WBAArenaBootstrap new 
		bootstrapGame: startingGame 
		on: screen