actions
upSelection
	"Called when the user presses the up key."
	currentChoice := currentChoice - 1.
	currentChoice = 0 ifTrue: [ currentChoice := self numArenas ].