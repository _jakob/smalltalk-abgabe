rendering
renderWidgetsOn: canvas
	"This is called every tick."
	titleWidget renderOn: canvas translatedBy: 210@30.
	canvas pathTransform translateBy: -40@90.
	arenaWidget renderOn: canvas translatedBy: 100@0.
	arrowWidget renderOn: canvas translatedBy: -75 @ (currentChoice * 80 - 70)