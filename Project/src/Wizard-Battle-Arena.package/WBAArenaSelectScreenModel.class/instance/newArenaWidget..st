screen building
newArenaWidget: numArenas
	"Loads the number of available arenas into the select menu."
	^ self newColumnWidget: ((1 to: numArenas) collect: [ :i | self newSubArenaWidget: i ]) space: 80