events
visitWindowCloseEvent: anEvent
	"Closes the window of the game."
	self terminateProcesses.
	
	Smalltalk snapshot: false andQuit: true
	