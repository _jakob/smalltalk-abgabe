# What is this ?

This is a fork from the [WizardBattleArena](https://github.com/clementbera/wizard-battle-arena) game, made for a practical asignment at TU Wien.

# Package overview

We only care about the `Wizard-Battle-Arena.package`.

## Loading and running the game

I've already added an image file that contains the src files as repository. 

*Running*

Open the Pharo-Ui: `cd wizard-battle-arena/Project && ./pharo-ui Pharo.image`

Run the game:
Left click in the background --> Playground and in there type: 

	WizardBattleArena start

And then run with the green arrow
